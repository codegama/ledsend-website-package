@extends('layouts.admin')

@section('page_header',tr('users'))

@section('styles')

<link rel="stylesheet" href="{{asset('admin-assets/css/dropify.min.css')}}">

<link href="{{asset('admin-assets/css/datepicker.css')}}" rel="stylesheet">

@endsection

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.view')}}">{{tr('view_users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)" aria-current="page"></a><span>{{tr('add_billing_accounts')}}</span></li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">
            <a class="m-b-0 text-white" href="{{route('admin.users.index',['user_id'=> Request::get('user_id')?? $user_details->user_id])}}">{{tr('add_billing_accounts')}} - {{$user_details->username??''}} </a>

        
            <a class="btn btn-secondary pull-right" href="{{route('admin.user_billing_accounts.index',['user_id'=> Request::get('user_id')?? $user_details->user_id])}}">
                <i class="fa fa-eye"></i> {{tr('view_billing_accounts')}}
            </a>
        </h4>

    </div>

    @include('admin.users.billing_accounts._form')

</div>

@endsection



