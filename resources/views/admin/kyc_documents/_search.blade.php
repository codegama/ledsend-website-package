<div class="col-md-12 mb-2">

        <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.kyc_documents.index')}}" method="GET" role="search">

            <div class="row input-group">
                <div class="col-md-4"></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="search_key" placeholder="{{tr('document_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn"></span>
                </div>

                <div class="col-md-2">

                    <select class="form-control" name="status">

                        <option value="">{{tr('select_status')}}</option>
                        <option value="{{APPROVED}}" @if(Request::get('status')== APPROVED && Request::get('status')!='') selected @endif>{{tr('approved')}}</option>
                        <option value="{{DECLINED}}" @if(Request::get('status')== DECLINED && Request::get('status')!='') selected @endif>{{tr('declined')}}</option>
                       

                    </select>
                </div>


                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                </button> &nbsp;&nbsp;
                <a class="btn btn-danger" href="{{route('admin.kyc_documents.index')}}">{{tr('clear')}}</a>
            </div>

        </form>
</div>