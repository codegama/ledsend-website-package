<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Jobs\SendEmailJob;

use \App\Repositories\PaymentRepository as PaymentRepo;

use App\Admin, App\User, App\KycDocument, App\UserWallet, App\UserWalletPayment,App\UserDispute;

use App\GeneratedInvoice, App\GeneratedInvoiceInfo, App\GeneratedInvoiceItem, App\Inbox;

use App\Settings, App\StaticPage;

use App\ResolutionCenter;

use Carbon\Carbon;

class AdminController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    /**
     * @method index()
     *
     * @uses Show the application dashboard.
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function index() {
        
        $data = new \stdClass();
        
        $data->total_users = \App\User::count();

        $data->recent_users = \App\User::orderBy('created_at','DESC')->skip(0)->take(7)->get();

        $data->total_wallet = \App\UserWallet::sum('total');

        $data->today_wallet = \App\UserWallet::whereDate('created_at', Carbon::today())->sum('total');

        $data->total_withdraw_requests = \App\UserWithdrawal::count();

        $view = last_days(10);

        $views = array();

        foreach($view['get'] as $key=>$date){
            
            $views[$key]['x'] = date('d M', strtotime($date->created_at)); 

            $views[$key]['y'] = $date->count; 
        }


        $wallet_details = wallet_details();
        
        
        return view('admin.dashboard')
                ->with('main_page','dashboard')
                ->with('view',$view)
                ->with('views',$views)
                ->with('wallet_details',$wallet_details)
                ->with('data', $data);
    
    }

    /**
     * @method users_index()
     *
     * @uses To list out users details 
     *
     * @created Ganesh
     *
     * @updated Ganesh
     *
     * @param 
     * 
     * @return return view page
     *
     */

    public function users_index(Request $request) {

        $base_query = \App\User::orderBy('updated_at','desc');


        if($request->search_key) {

            $base_query->where(function ($query) use ($request) {
                $query->where('name', "like", "%" . $request->search_key . "%");
                $query->orWhere('email', "like", "%" . $request->search_key . "%");
                $query->orWhere('mobile', "like", "%" . $request->search_key . "%");
            });
        }
        
        if($request->kyc_status != '') {

            $kyc_status = $request->kyc_status == YES ? [USER_KYC_DOCUMENT_APPROVED] : [USER_KYC_DOCUMENT_NONE, USER_KYC_DOCUMENT_PENDING, USER_KYC_DOCUMENT_DECLINED];

            $base_query->whereIn('is_kyc_document_approved', $kyc_status);

        }

        if($request->status!='') {

            $base_query->where('status',$request->status);
  
        }


        $users = $base_query->paginate(10);
        
        return view('admin.users.index')
                    ->with('main_page','users_crud')
                    ->with('page','users')
                    ->with('sub_page', 'users-index')
                    ->with('users', $users);
    }

    /**
     * @method users_create()
     *
     * @uses To create a user details
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_create() {

        $user_details = new User;

        return view('admin.users.create')
                    ->with('main_page','users_crud')
                    ->with('page', 'users')
                    ->with('sub_page' , 'users-create')
                    ->with('user_details', $user_details);           
    }

    /**
     * @method users_edit()
     *
     * @uses To display and update user details based on the user id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param integer $user_id
     * 
     * @return redirect view page 
     *
     */
    public function users_edit(Request $request) {

        try {

            $user_details = \App\User::find($request->user_id);

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);
            }

            return view('admin.users.edit')
                    ->with('main_page','users_crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-index')
                    ->with('user_details' , $user_details);
            
        } catch(Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error',  $e->getMessage());
        }
    
    }


    /**
     * @method users_save()
     *
     * @uses To save the users details of new/existing user object based on details
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param object user Form Data
     *
     * @return success message
     *
     */
    public function users_save(Request $request) {
        
        try {

            DB::begintransaction();

            $validator = Validator::make( $request->all(), [
                'first_name' => 'required|max:255|min:2',
                'last_name' => 'required|max:255|min:2',
                'email' => $request->user_id ? 'required|email|max:191|unique:users,email,'.$request->user_id.',id' : 'required|email|max:191|unique:users,email,NULL,id',
                'password' => $request->user_id ? "" : 'required|min:6|confirmed',
                'mobile' => 'required|digits_between:6,13',
                'picture' => 'mimes:jpg,png,jpeg',
                'description' => 'max:191'
                ]
            );

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);

            }

            $user_details = $request->user_id ? \App\User::find($request->user_id) : new User;

            $is_new_user = NO;

            if($user_details->id) {

                $message = tr('user_updated_success'); 

            } else {

                $is_new_user = YES;

                $user_details->password = ($request->password) ? \Hash::make($request->password) : null;

                $message = tr('user_created_success');

                $user_details->email_verified_at = date('Y-m-d H:i:s');

                $user_details->picture = asset('placeholder.jpg');

                $user_details->is_verified = USER_EMAIL_VERIFIED;

                $user_details->login_by = LOGIN_BY_MANUAL;
                
                $user_details->device_type = DEVICE_WEB ;

            }

            $user_details->first_name = $request->first_name ?: $user_details->first_name;

            $user_details->last_name = $request->last_name ?: $user_details->last_name;
            
            $user_details->email = $request->email ?: $user_details->email;

            $user_details->mobile = $request->mobile ?: '';

            $user_details->gender = $request->gender ?? 'male';

            $user_details->about = $request->about ?: '';

            $user_details->login_by = $request->login_by ?: 'manual';

            $user_details->timezone = $request->timezone?:'';


            // Upload picture

            if($request->hasFile('picture') ) {

                if($request->user_id) {

                    Helper::storage_delete_file($user_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $user_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }


            if($user_details->save()) {

                if($is_new_user == YES) {

                    $email_data['subject'] = tr('user_welcome_title').' '.Setting::get('site_name');

                    $email_data['page'] = "emails.users.welcome";

                    $email_data['data'] = $user_details;

                    $email_data['email'] = $user_details->email;

                    $email_data['password'] = $request->password;

                    $this->dispatch(new SendEmailJob($email_data));

                }
              

                DB::commit(); 

                return redirect(route('admin.users.view', ['user_id' => $user_details->id]))->with('flash_success', $message);
            } 

            throw new Exception(tr('user_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }    
     }


    /**
     * @method users_view()
     *
     * @uses view the users details based on users id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param integer $user_id
     * 
     * @return View page
     *
     */
    public function users_view(Request $request) {
        
        try {

            $user_details = \App\User::where('id', $request->user_id)->first();

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);                
            }            
                 
            return view('admin.users.view')
                        ->with('main_page','users_crud')
                        ->with('page', 'users') 
                        ->with('sub_page','users-index') 
                        ->with('user_details' , $user_details);

            
        } catch (Exception $e) {

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }
    }

    /**
     * @method users_delete()
     *
     * @uses delete the user details based on user id
     *
     * @created Ganesh
     *
     * @updated  
     *
     * @param integer $request id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function users_delete(Request $request) {
            
        try {

            DB::begintransaction();

            $user_details = \App\User::where('id',$request->user_id)->first();

            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);                
            }

            if($user_details->delete()) {

                DB::commit();

                return redirect()->route('admin.users.index')->with('flash_success',tr('user_deleted_success'));   

            } 
            
            throw new Exception(tr('user_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        }       
         
    }


    /**
     * @method users_status
     *
     * @uses To delete the users details based on users id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param integer $users_id
     * 
     * @return response success/failure message
     *
     **/
    public function users_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = \App\User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);
                
            }

            $user_details->status = $user_details->status ? DECLINED : APPROVED;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->status ? tr('user_approve_success') : tr('user_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method users_verify_status()
     *
     * @uses verify for the user
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */
    public function users_verify_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = \App\User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_details_not_found'), 101);
                
            }

            $user_details->is_verified = $user_details->is_verified ? USER_EMAIL_NOT_VERIFIED : USER_EMAIL_VERIFIED;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->is_verified ? tr('user_verify_success') : tr('user_unverify_success');

                return redirect()->route('admin.users.index')->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_verify_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     *
     * @method settings_control()
     *
     * @uses used to view the settings page
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param - 
     *
     * @return view page
     */

    public function settings_control() {

        return view('admin.settings.control')->with('main_page', 'settings');
   
    }

    /**
     *
     * @method razorpay_settings_control()
     *
     * @uses used to view the settings page
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param - 
     *
     * @return view page
     */

    public function razorpay_settings_control() {

        return view('admin.settings.razorpay')->with('main_page', 'settings');
   
    }
    
    

    /**
     *
     * @method settings()
     *
     * @uses used to view the settings page
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param - 
     *
     * @return view page
     */

    public function settings() {

        $env_values = EnvEditorHelper::getEnvValues();

        return view('admin.settings.settings')
                ->with('env_values',$env_values)
                ->with('main_page' , 'settings');
   
    }

    /**
     * @method settings_save()
     * 
     * @uses to update settings details
     *
     * @created Ganesh
     *
     * @updated Ganesh
     *
     * @param (request) setting details
     *
     * @return success/error message
     */
    public function settings_save(Request $request) {

        try {
            
            DB::beginTransaction();
            
            $validator = Validator::make($request->all() , 
                [
                    'site_logo' => 'mimes:jpeg,jpg,bmp,png',
                    'site_icon' => 'mimes:jpeg,jpg,bmp,png',

                ],
                [
                    'mimes' => tr('image_error')
                ]
            );

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
            }

            foreach( $request->toArray() as $key => $value) {

                if($key != '_token') {

                    $check_settings = Settings::where('key' ,'=', $key)->count();

                    if( $check_settings == 0 ) {

                        throw new Exception( $key.tr('settings_key_not_found'), 101);
                    }
                    
                    if( $request->hasFile($key) ) {
                                            
                        $file = Settings::where('key' ,'=', $key)->first();
                       
                        Helper::delete_file($file->value, FILE_PATH_SITE);

                        $file_path = Helper::storage_upload_file($request->file($key) , FILE_PATH_SITE);    

                        $result = Settings::where('key' ,'=', $key)->update(['value' => $file_path]); 

                        if( $result == TRUE ) {
                     
                            DB::commit();
                   
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 
                   
                    } else {
                    
                        $result = Settings::where('key' ,'=', $key)->update(['value' => $value]);  
                    
                        if( $result == TRUE ) {
                         
                            DB::commit();
                       
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 

                    }  
 
                }
            }

            Helper::settings_generate_json();

            return back()->with('flash_success', tr('settings_update_success'));
            
        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method profile()
     *
     * @uses  Used to display the logged in admin details
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function profile() {

        $admin_details = Auth::guard('admin')->user();

        return view('admin.account.profile')
                ->with('main_page' , 'profile')
                ->with('admin_details',$admin_details);

    }

    /**
     * @method profile_save()
     *
     * @uses Used to update the admin details
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */

    public function profile_save(Request $request) {
       
        try {

            DB::beginTransaction();

            $validator = Validator::make( $request->all(), [
                    'name' => 'max:191',
                    'email' => $request->admin_id ? 'email|max:191|unique:admins,email,'.$request->admin_id : 'email|max:191|unique:admins,email,NULL',
                    'admin_id' => 'required|exists:admins,id',
                    'picture' => 'mimes:jpeg,jpg,png'
                ]
            );
            
            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
                
            }

            $admin_details = Admin::find($request->admin_id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();

                throw new Exception(tr('admin_details_not_found'), 101);

            }
        
            $admin_details->name = $request->name ?: $admin_details->name;

            $admin_details->email = $request->email ?: $admin_details->email;

            $admin_details->mobile = $request->mobile ?: $admin_details->mobile;

            $admin_details->about = $request->about ?: $admin_details->about;
 

            if($request->hasFile('picture') ) {
                
                Helper::storage_delete_file($admin_details->picture, PROFILE_PATH_ADMIN); 
                
                $admin_details->picture = Helper::storage_upload_file($request->file('picture'), PROFILE_PATH_ADMIN);
            }
            
            $admin_details->remember_token = Helper::generate_token();

            $admin_details->timezone = $request->timezone ?: $admin_details->timezone;

            $admin_details->save();

            DB::commit();

            return redirect()->route('admin.profile')->with('flash_success', tr('admin_profile_success'));


        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error' , $e->getMessage());

        }    
    
    }

    /**
     * @method change_password()
     *
     * @uses  Used to change the admin password
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function change_password(Request $request) {

        try {
           
            DB::begintransaction();

            $validator = Validator::make($request->all(), [              
                'password' => 'required|min:6',
                'old_password' => 'required',
                'confirm_password' => 'required|min:6'
            ]);

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
                
            }

            $admin_details = Admin::find(Auth::guard('admin')->user()->id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();

                throw new Exception(tr('admin_details_not_found'), 101);

            }

            if(Hash::check($request->old_password,$admin_details->password)) {

                $admin_details->password = Hash::make($request->password);

                $admin_details->save();

                DB::commit();

                Auth::guard('admin')->logout();

                return redirect()->route('admin.login')->with('flash_success', tr('password_change_success'));
                
            } else {

                throw new Exception(tr('password_mismatch'));
            }


        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error' ,$e->getMessage());

        }    
    
    }

    /**
     * @method kyc_documents_index()
     *
     * @uses To display document list page
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     *
     * @return view page
     */
    public function kyc_documents_index(Request $request) {

        $base_query = KycDocument::orderBy('updated_at','desc');

        if($request->status!=''){
        
         $base_query->where('status',$request->status);

        }

        if($request->search_key){

            $base_query = $base_query->where('name','LIKE','%'.$request->search_key.'%');
        }

        $kyc_documents = $base_query->get();

        return view('admin.kyc_documents.index')
                    ->with('main_page','lookup_management_documents')
                    ->with('page' , 'lookup_management_documents')
                    ->with('sub_page','kyc_documents_index')
                    ->with('kyc_documents' , $kyc_documents);
    }

    /**
     * @method kyc_documents_create()
     *
     * @uses To create document details
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param -
     *
     * @return view page
     */
    public function kyc_documents_create() {

        $kyc_document_details = new KycDocument;
        
        return view('admin.kyc_documents.create')
                ->with('main_page','lookup_management_documents')
                ->with('page' , 'lookup_management_documents')
                ->with('sub_page','kyc_documents_create')
                ->with('kyc_document_details', $kyc_document_details);
    }
  
    /**
     * @method kyc_documents_edit()
     *
     * @uses To display and update document details based on the document id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param object $request - document Id
     * 
     * @return redirect view page 
     *
     */
    public function kyc_documents_edit(Request $request) {

        $kyc_document_details = KycDocument::find($request->kyc_document_id);
       
        if(!$kyc_document_details) {

            return back()->with('flash_error', tr('document_not_found'));
        }

        return view('admin.kyc_documents.edit')
                    ->with('page','documents')
                    ->with('sub_page','documents-view')
                    ->with('kyc_document_details',$kyc_document_details);
    }

    /**
     * @method kyc_documents_save()
     *
     * @uses To save the details based on document or to create a new document
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param object $request - document object details
     * 
     * @return success/failure message
     *
     */
    public function kyc_documents_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [

                'name' => 'required|max:191',
                'description' => 'max:191'
            ];
            
            Helper::custom_validator($request->all(),$rules);
            
            $kyc_document_details = new KycDocument;

            $message = tr('document_created_success');

            if($request->kyc_document_id != '') {

                $kyc_document_details = KycDocument::find($request->kyc_document_id);

                $message = tr('document_updated_success');

                if($request->hasFile('picture')) {
                    Helper::storage_delete_file($request->file('picture'), DOCUMENTS_PATH);
                }

            } else {
               
                $kyc_document_details->status = APPROVED;

            }

            if($request->hasFile('picture')) {

                $kyc_document_details->picture = Helper::storage_upload_file($request->file('picture'), DOCUMENTS_PATH);

            }

            $kyc_document_details->name = $request->name ?: $kyc_document_details->name;
            
            $kyc_document_details->description = $request->description ?: '';

            if($kyc_document_details->save()) {

                DB::commit();

                return redirect()->route('admin.kyc_documents.view', ['kyc_document_id' => $kyc_document_details->id])->with('flash_success', $message);

            }

            return back()->with('flash_error', tr('document_save_failed'));
            
        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        }
        
    }

    /**
     * @method kyc_documents_view()
     *
     * @uses view the document details based on document id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - document Id
     * 
     * @return View page
     *
     */
    public function kyc_documents_view(Request $request) {

        $kyc_document_details = KycDocument::find($request->kyc_document_id);

        if(!$kyc_document_details) {

            return redirect()->route('admin.kyc_documents.index')->with('flash_error',tr('document_not_found'));

        }

        return view('admin.kyc_documents.view')
                    ->with('page', 'documents')
                    ->with('sub_page','documents-view')
                    ->with('kyc_document_details' , $kyc_document_details);
    
    }

    /**
     * @method documents_delete
     *
     * @uses To delete the document details based on selected document id
     *
     * @created Anjana
     *
     * @updated 
     *
     * @param integer $document_id
     * 
     * @return response of success/failure details
     *
     */
    public function kyc_documents_delete(Request $request) {

        try {

            DB::beginTransaction();

            $kyc_document_details = KycDocument::find($request->kyc_document_id);

            if(!$kyc_document_details) {

                throw new Exception(tr('document_not_found'), 101);
                
            }

            if($kyc_document_details->delete()) {

                DB::commit();

                return redirect()->route('admin.kyc_documents.index')->with('flash_success',tr('document_deleted_success')); 

            } 

            throw new Exception(tr('document_delete_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.kyc_documents.index')->with('flash_error', $e->getMessage());

        }
   
    }

    /**
     * @method documents_status()
     *
     * @uses To delete the document details based on document id
     *
     * @created Anjana
     *
     * @updated 
     *
     * @param integer $document_id
     * 
     * @return response success/failure message
     *
     */
    public function kyc_documents_status(Request $request) {

        try {

            DB::beginTransaction();

            $kyc_document_details = KycDocument::find($request->kyc_document_id);

            if(!$kyc_document_details) {

                throw new Exception(tr('document_not_found'), 101);
                
            }

            $kyc_document_details->status = $kyc_document_details->status ? DECLINED : APPROVED;

            if( $kyc_document_details->save()) {

                DB::commit();

                $message = $kyc_document_details->status ? tr('document_approve_success') : tr('document_decline_success');

                return redirect()->back()->with('flash_success', $message);

            } 

            throw new Exception(tr('document_status_change_failed'));
                
        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.kyc_documents.index')->with('flash_error', $e->getMessage());
        }

    }
     
    /**
     * @method static_pages_index()
     *
     * @uses Used to list the static pages
     *
     * @created Ganesh
     *
     * @updated   
     *
     * @param -
     *
     * @return List of pages   
     */

    public function static_pages_index(Request $request) {

        $base_query = StaticPage::orderBy('updated_at' , 'desc');

        if($request->status!=''){
        
        $base_query->where('status',$request->status);
   
        }
   
        if($request->search_key){
   
        $base_query = $base_query->where('title','LIKE','%'.$request->search_key.'%')
                                  ->orWhere('type','LIKE','%'.$request->search_key.'%');

        }

        $static_pages = $base_query->paginate(10);

        return view('admin.static-pages.index')
                    ->with('main_page','static_pages_crud')
                    ->with('page','static_pages')
                    ->with('sub_page','static_pages_view')
                    ->with('static_pages', $static_pages);
    
    }

    /**
     * @method static_pages_create()
     *
     * @uses To create static_page details
     *
     * @created Ganesh
     *
     * @updated    
     *
     * @param
     *
     * @return view page   
     *
     */
    public function static_pages_create() {

        $static_keys = ['about' , 'contact', 'privacy' , 'terms' , 'help' , 'faq' , 'refund', 'cancellation', 'business'];

        foreach ($static_keys as $key => $static_key) {

            // Check the record exists

            $check_page = StaticPage::where('type', $static_key)->first();

            if($check_page) {
                unset($static_keys[$key]);
            }
        }

        $static_keys[] = 'others';

        $static_page_details = new StaticPage;


        return view('admin.static-pages.create')
                ->with('main_page','static_pages_crud')
                ->with('page','static_pages')
                ->with('sub_page',"static_pages_create")
                ->with('static_keys', $static_keys)
                ->with('static_page_details',$static_page_details);
   
    }

    /**
     * @method static_pages_edit()
     *
     * @uses To display and update static_page details based on the static_page id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param object $request - static_page Id
     * 
     * @return redirect view page 
     *
     */
    public function static_pages_edit(Request $request) {

        try {

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
            }

            $static_keys = ['about', 'contact', 'privacy', 'terms', 'help', 'faq', 'refund', 'cancellation', 'business'];

            foreach ($static_keys as $key => $static_key) {

                // Check the record exists

                $check_page = StaticPage::where('type', $static_key)->first();

                if($check_page) {
                    unset($static_keys[$key]);
                }
            }

            $static_keys[] = 'others';

            $static_keys[] = $static_page_details->type;

            return view('admin.static-pages.edit')
                    ->with('main_page','static_pages_crud')
                    ->with('page' , 'static_pages')
                    ->with('sub_page','static_pages_view')
                    ->with('static_keys' , array_unique($static_keys))
                    ->with('static_page_details' , $static_page_details);
            
        } catch(Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.static_pages.index')->with('flash_error' , $error);

        }
    }

    /**
     * @method static_pages_save()
     *
     * @uses Used to create/update the page details 
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param
     *
     * @return index page    
     *
     */
    public function static_pages_save(Request $request) {

        try {
            
            DB::beginTransaction();

            $validator = Validator::make($request->all(), [              
                'description' => 'required',
                'type' => !$request->static_page_id ? 'required' : "",
                'title' => $request->static_page_id ? 'required|max:255|unique:static_pages,title,'.$request->static_page_id : 'required|max:255|unique:static_pages,title',

            ]);
            
            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
                
            }
            if($request->static_page_id != '') {

                $static_page_details = StaticPage::find($request->static_page_id);

                $message = tr('static_page_updated_success');                    

            } else {

                $check_page = "";

                // Check the staic page already exists

                if($request->type != 'others') {

                    $check_page = StaticPage::where('type',$request->type)->first();

                    if($check_page) {

                        return back()->with('flash_error',tr('static_page_already_alert'));
                    }

                }

                $message = tr('static_page_created_success');

                $static_page_details = new StaticPage;

                $static_page_details->status = APPROVED;

            }

            $static_page_details->title = $request->title ?: $static_page_details->title;

            $static_page_details->description = $request->description ?: $static_page_details->description;

            $static_page_details->type = $request->type ?? $static_page_details->type;

            $unique_id = $request->type ?? $static_page_details->type;

            if(!in_array($request->type ?? $static_page_details->type, ['about', 'contact', 'privacy', 'terms', 'help', 'faq', 'refund', 'cancellation', 'business'])) {

                $unique_id = routefreestring($request->title ?? rand());

                $unique_id = in_array($unique_id, ['about', 'contact', 'privacy', 'terms', 'help', 'faq', 'refund', 'cancellation', 'business']) ? $unique_id.rand() : $unique_id;

            }

            $static_page_details->unique_id = $unique_id ?? rand();

            if($static_page_details->save()) {

                DB::commit();
                
                Helper::settings_generate_json();

                return redirect()->route('admin.static_pages.view', ['static_page_id' => $static_page_details->id] )->with('flash_success', $message);

            } 

            throw new Exception(tr('static_page_save_failed'), 101);
                      
        } catch(Exception $e) {

            DB::rollback();

            return back()->withInput()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method static_pages_delete()
     *
     * Used to view file of the create the static page 
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param -
     *
     * @return view page   
     */

    public function static_pages_delete(Request $request) {

        try {

            DB::beginTransaction();

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
                
            }

            if($static_page_details->delete()) {

                DB::commit();

                return redirect()->route('admin.static_pages.index')->with('flash_success',tr('static_page_deleted_success')); 

            } 

            throw new Exception(tr('static_page_error'));

        } catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->route('admin.static_pages.index')->with('flash_error', $error);

        }
    
    }

    /**
     * @method static_pages_view()
     *
     * @uses view the static_pages details based on static_pages id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - static_page Id
     * 
     * @return View page
     *
     */
    public function static_pages_view(Request $request) {

        $static_page_details = StaticPage::find($request->static_page_id);

        if(!$static_page_details) {
           
            return redirect()->route('admin.static_pages.index')->with('flash_error',tr('static_page_not_found'));
        }

        return view('admin.static-pages.view')
                    ->with('main_page', 'static_pages_crud')
                    ->with('page', 'static_pages')
                    ->with('sub_page', 'static_pages_view')
                    ->with('static_page_details', $static_page_details);
    }

    /**
     * @method static_pages_status_change()
     *
     * @uses To update static_page status as DECLINED/APPROVED based on static_page id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param - integer static_page_id
     *
     * @return view page 
     */

    public function static_pages_status_change(Request $request) {

        try {

            DB::beginTransaction();

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
            }

            $static_page_details->status = $static_page_details->status == DECLINED ? APPROVED : DECLINED;

            $static_page_details->save();

            DB::commit();

            $message = $static_page_details->status == DECLINED ? tr('static_page_decline_success') : tr('static_page_approve_success');

            return redirect()->back()->with('flash_success', $message);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }


    /**
     * @method user_wallets_index()
     * 
     * @uses to display the wallet details for the user
     * 
     * @created Ganesh
     *
     * @updated 
     *  
     * @param 
     *
     * @return success/failure message
     */
    public function user_wallets_index(Request $request) {

        $base_query = \App\UserWallet::orderBy('user_wallets.created_at','desc');

        if($request->search_key){

            $base_query = $base_query
                    ->where('name','LIKE','%'.$request->search_key.'%')
                    ->orWhere('email','LIKE','%'.$request->search_key.'%')
                    ->orWhere('mobile','LIKE','%'.$request->search_key.'%');
        }
        
        if($request->today_wallet){

            $base_query->whereDate('user_wallets.created_at', Carbon::today());

        }

        $user_wallets = $base_query->commonResponse()->paginate(10);

        return view('admin.wallets.index')
                ->with('main_page', 'wallets_management')
                ->with('page','user_wallets')
                ->with('sub_page', 'user_wallets-index')
                ->with('user_wallets', $user_wallets);
    
    }

    /**
     * @method user_wallets_view()
     * 
     * @uses To display wallet dashboard
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     *
     * @return view page
     */
    public function user_wallets_view(Request $request) {

        try {

            $user_wallet_details = \App\UserWallet::where('user_id', $request->user_id)->first();

            $user_wallet_payments = UserWalletPayment::where('user_id', $request->user_id)->orderBy('user_wallet_payments.id', 'desc')->paginate(10);

            return view('admin.wallets.view')
                    ->with('main_page','wallets_management')
                    ->with('page','user_wallets')
                    ->with('sub_page','user_wallets-index')
                    ->with('user_wallet_details',$user_wallet_details)
                    ->with('user_wallet_payments',$user_wallet_payments);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error',$e->getMessage());
        }

    }


    /**
     * @method user_wallet_payments()
     * 
     * @uses to display the wallet trasaction for the all user
     * 
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     *
     * @return success/failure message
     */
    public function user_wallet_payments(Request $request) {

        $base_query = UserWalletPayment::orderBy('created_at','desc');

        $sub_page = 'user_wallets-transactions';

        if($request->search_key){

            $base_query->where('payment_id','LIKE','%'.$request->search_key.'%');
                 
        }

        if($request->payment_mode){

            $base_query->where('payment_mode',$request->payment_mode);
                 
        }

        if($request->payment_amount_type){

            $base_query->where('payment_type',$request->payment_amount_type);
        }

        if($request->user_id){

            $base_query->where('user_id',$request->user_id);
        }


        if($request->has('type')){

            $base_query->where('payment_mode',$request->type);

            $sub_page = 'bank_transactions';
        }

        $user_wallet_payments = $base_query->paginate(10);

        return view('admin.wallets.payments')
                ->with('main_page', 'wallets_management')
                ->with('page','user_wallets')
                ->with('sub_page', $sub_page)
                ->with('user_wallet_payments', $user_wallet_payments)
                ->with('requests',$request->all());
    }

    /**
     * @method user_wallet_payments_view()
     * 
     * @uses to display the wallet trasaction details for the  specified user
     * 
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     *
     * @return success/failure message
     */
    public function user_wallet_payments_view(Request $request) {
        
        try {

            $user_wallet_payment_details = UserWalletPayment::where('id', $request->user_wallet_payment_id)->first();
          
            if(!$user_wallet_payment_details) {

                throw new Exception(tr('wallet_trasaction_details_not_found'), 101);
            }
            
            return view('admin.wallets.payments-view')
                    ->with('page','user_wallets')
                    ->with('sub_page','user_wallets-index')
                    ->with('wallet_payment_details', $user_wallet_payment_details);

        } catch(Exception $e) {

            return redirect()->route('admin.user_wallet_payments.index')->with('flash_error',$e->getMessage());
        }

    }

    /**
     * @method document_verify_status()
     *
     * @uses verify for the user
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */
    public function document_verify_status(Request $request) {

        try {
            
            DB::beginTransaction();

            $user_details = \App\User::find($request->user_id);


            if(!$user_details) {

                throw new Exception(tr('user_details_not_found'), 101);
                
            }

            if(in_array($user_details->is_kyc_document_approved, [USER_KYC_DOCUMENT_NONE, USER_KYC_DOCUMENT_PENDING])) {

                $user_details->is_kyc_document_approved = USER_KYC_DOCUMENT_APPROVED;

            } else {

                $user_details->is_kyc_document_approved = USER_KYC_DOCUMENT_DECLINED;
            }

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->is_kyc_document_approved ? tr('user_kyc_verify_success') : tr('user_kyc_unverify_success');

                    /**
                    * @todo Verify mail to user
                    */
                    // $job_data['user_details'] = $user_details;

                    // $job_data['email_verification'] = YES;

                    // $this->dispatch(new UserStatusJob($job_data));

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_kyc_verify_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method unverified_user_index()
     *
     * @uses verify for the user
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */

    public function unverified_users_index(Request $request){

        $base_query = \App\User::orderBy('updated_at','desc');
        
        if($request->search_key) {

            $base_query->where(function ($query) use ($request) {
                $query->where('name', "like", "%" . $request->search_key . "%");
                $query->orWhere('email', "like", "%" . $request->search_key . "%");
                $query->orWhere('mobile', "like", "%" . $request->search_key . "%");
            });
        }

        if($request->status!=''){

            $base_query->where('status', $request->status);
        }


        $users = $base_query->where('is_kyc_document_approved', '!=', USER_KYC_DOCUMENT_APPROVED)->paginate(10);

        foreach($users as $user){

            $user->documents_count = \App\UserKycDocument::where('user_id',$user->id)->count();
            
        }
        
        return view('admin.users.unverified_users')
                    ->with('main_page','user_unverified')
                    ->with('users', $users);
    
    }

    /**
     * @method user_kyc_documents_view()
     *
     * @uses view the provider document list, based on user Id / document id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */
    
    public function user_kyc_documents_view(Request $request) {
       
        try {

        $user_kyc_documents = \App\UserKycDocument::where('user_id',$request->user_id)->orderBy('updated_at','desc')->paginate(10);
        
        $user_details = \App\User::find($request->user_id);

        if(!$user_details){

            throw new Exception(tr('user_details_not_found'));

        }

        return view('admin.users.documents.index')
                    ->with('main_page','user_unverified')
                    ->with('user_details',$user_details)
                    ->with('user_kyc_documents' , $user_kyc_documents);

        } catch(Exception $e) {
            
            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method generated_invoices()
     *
     * @uses list the generated invoices of user
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */

    public function generated_invoices_index(Request $request) {

        
        $base_query = GeneratedInvoice::orderBy('updated_at','desc');

        $sub_page = 'invoices-view';

        if($request->status!='') {

            $base_query = $base_query->where('status',$request->status);
        }

        $search_key = $request->search_key;

        if($search_key) {

            $base_query
            ->whereHas('fromUser', function ($query) use ($search_key) {
                $query
                ->where('users.name', 'like', '%' .$search_key . '%');
             })
             ->orWhereHas('toUser', function ($query) use ($search_key) {
                $query
                ->where('users.name', 'like', '%' .$search_key . '%');
             })
             ->orWhere(function ($query) use ($request) {
                $query->where('invoice_number', "like", "%" . $request->search_key . "%");
                $query->orWhere('title', "like", "%" . $request->search_key . "%");
            });
            
             
        }

        
        if($request->pending_invoices){
            
            $sub_page = 'invoices-pending';

            $base_query = $base_query->whereIn('status',[INVOICE_DRAFT,INVOICE_SCHEDULED]);
        }

        $generated_invoices = $base_query->paginate(10);
        

        foreach($generated_invoices  as $invoice){

           $invoice->from_user =  \App\User::find($invoice->sender_id)??'';

           $invoice->to_user =  \App\User::find($invoice->to_user_id)??'';

        }


        return view('admin.invoices.index')
        ->with('main_page','invoices_management')
        ->with('page','invoices')
        ->with('sub_page',$sub_page)
        ->with('generated_invoices' , $generated_invoices);

    }

    /**
     * @method generated_invoices_view()
     *
     * @uses view the generated invoices of user
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */

    public function generated_invoices_view(Request $request) {

        try {
      
        $generated_invoice = GeneratedInvoice::find($request->generated_invoice_id);

        $generated_invoice->from_user = \App\User::find($generated_invoice->sender_id)??'';

        $generated_invoice->to_user = \App\User::find($generated_invoice->to_user_id)??'';

        $generated_invoice_info =  GeneratedInvoiceInfo::where('generated_invoice_id',$request->generated_invoice_id)->first();
        
        $generated_invoice_items = GeneratedInvoiceItem::where('generated_invoice_id',$request->generated_invoice_id)->get();
        
        
        return view('admin.invoices.view')
        ->with('main_page','invoices_management')
        ->with('page','invoices')
        ->with('invoice_details',$generated_invoice)
        ->with('invoice_info',$generated_invoice_info)
        ->with('invoice_items',$generated_invoice_items)
        ->with('sub_page','invoices-view');

        } catch(Exception $e) {

            return redirect()->route('admin.generated_invoices.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method generated_invoices_delete()
     *
     * @uses delete the generated invoices of user
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */

    public function generated_invoices_delete(Request $request) {

        try {

            DB::beginTransaction();

            $generated_invoice = GeneratedInvoice::find($request->generated_invoice_id);

            if(!$generated_invoice) {

                throw new Exception(tr('static_page_not_found'), 101);
                
            }
           
            if($generated_invoice->delete()) {

                DB::commit();

                return redirect()->route('admin.generated_invoices.index')->with('flash_success',tr('invoice_delete_success')); 

            } 

            throw new Exception(tr('invoice_delete_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.generated_invoices.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_billing_accounts_create()
     *
     * @uses add billing accounts to user
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */

    public function user_billing_accounts_create(Request $request){

        $user_details = \App\User::find($request->user_id); 

        return view('admin.users.billing_accounts.create')
        ->with('main_page','users_crud')
        ->with('page', 'users')
        ->with('sub_page' , 'users-index')
        ->with('user_details', $user_details);   


    }

    /**
     * @method user_billing_accounts_save()
     *
     * @uses To save the users billing account details of new/existing user object based on details
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param object user Form Data
     *
     * @return success message
     *
     */
    public function user_billing_accounts_save(Request $request) {
        
        try {
            
            DB::begintransaction();

            $validator = Validator::make( $request->all(), [
                'account_holder_name' => 'required|max:30',
                'account_number' => $request->billing_account_id ? 'required|digits_between:6,13|unique:user_billing_accounts,account_number,'.$request->billing_account_id.',id' : 'required|digits_between:6,13|unique:user_billing_accounts,account_number,id',
                'ifsc_code'=>'required|regex:"^[A-Z]{4}0[A-Z0-9]{6}$"',
                'swift_code'=>'required',
                ]
            );

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);

            }

            $account_details = \App\UserBillingAccount::find($request->billing_account_id) ?? new \App\UserBillingAccount;


            $message = $account_details->id ? tr('user_account_updated_success') : tr('user_account_created_success');

            $account_details->nickname = $request->nickname ?: $account_details->nickname;

            $account_details->user_id = $request->user_id ?: $account_details->user_id;

            $account_details->bank_name = $request->bank_name ?: $account_details->bank_name;

            $account_details->account_holder_name = $request->account_holder_name ?: $account_details->account_holder_name;
            
            $account_details->account_number = $request->account_number ?: $account_details->account_number;

            $account_details->ifsc_code = $request->ifsc_code ?: $account_details->ifsc_code;

            $account_details->swift_code = $request->swift_code ?: $account_details->swift_code;

            if($account_details->save()) {

                DB::commit(); 

                return redirect(route('admin.user_billing_accounts.view', ['billing_account_id' => $account_details->id]))->with('flash_success', $message);
            } 

            throw new Exception(tr('user_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }    
    }

    /**
     * @method user_billing_accounts_index()
     *
     * @uses list the billing accounts of user
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */

    public function user_billing_accounts_index(Request $request){
        
        try{

        $base_query = \App\UserBillingAccount::orderBy('updated_at','desc');

        if($request->search_key){

                $base_query->where(function ($query) use ($request) {
                    $query->where('account_holder_name', "like", "%" . $request->search_key . "%");
                    $query->orWhere('account_number', "like", "%" . $request->search_key . "%");
                    $query->orWhere('ifsc_code', "like", "%" . $request->search_key . "%");
                });
        }

        $user_billing_accounts =  $base_query->where('user_id',$request->user_id)->paginate(10);

        $user_details = \App\User::find($request->user_id);

        return view('admin.users.billing_accounts.index')
                    ->with('main_page','users_crud')
                    ->with('page','users')
                    ->with('sub_page', 'users-index')
                    ->with('user_details' , $user_details)
                    ->with('user_billing_accounts' , $user_billing_accounts);
        } catch (Exception $e) {

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_billing_accounts_edit()
     *
     * @uses To display and update user details based on the user id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param integer $user_id
     * 
     * @return redirect view page 
     *
     */
    public function user_billing_accounts_edit(Request $request) {

        try {

            $user_billing_account_details = \App\UserBillingAccount::find($request->billing_account_id);

            if(!$user_billing_account_details) { 

                throw new Exception(tr('user_not_found'), 101);
            }

            return view('admin.users.billing_accounts.edit')
                    ->with('main_page','users_crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-index')
                    ->with('user_details' , $user_billing_account_details);
            
        } catch(Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error',  $e->getMessage());
        }
    
    }

    /**
     * @method user_billing_accounts_view()
     *
     * @uses view the users details based on users id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param integer $user_id
     * 
     * @return View page
     *
     */
    public function user_billing_accounts_view(Request $request) {
        
        try {

            $user_billing_account_details = \App\UserBillingAccount::where('id', $request->billing_account_id)->first();
           
            if(!$user_billing_account_details) { 

                throw new Exception(tr('user_account_not_found'), 101);                
            }            
                 
            return view('admin.users.billing_accounts.view')
                        ->with('main_page','users_crud')
                        ->with('page', 'users') 
                        ->with('sub_page','users-index') 
                        ->with('user_account_details' , $user_billing_account_details);

            
        } catch (Exception $e) {

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_billing_accounts_delete()
     *
     * @uses delete the billing account of user
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param object $request - user Id
     * 
     * @return View page
     *
     */
    public function user_billing_accounts_delete(Request $request) {

        try {

            DB::beginTransaction();

            $user_billing_account = \App\UserBillingAccount::find($request->billing_account_id);

            if(!$user_billing_account) {

                throw new Exception(tr('user_account_not_found'), 101);
                
            }
           
            if($user_billing_account->delete()) {

                DB::commit();

                return redirect()->route('admin.user_billing_accounts.index',['user_id' => $user_billing_account->user_id])->with('flash_success',tr('account_delete_success')); 

            } 

            throw new Exception(tr('billing_account_delete_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.generated_invoices.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_billing_accounts_status
     *
     * @uses To approve/decline the user accounts
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param integer $users_id
     * 
     * @return response success/failure message
     *
     **/
    public function user_billing_accounts_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_billing_account_details = \App\UserBillingAccount::find($request->billing_account_id);

            if(!$user_billing_account_details) {

                throw new Exception(tr('user_not_found'), 101);
                
            }

            $user_billing_account_details->status = $user_billing_account_details->status ? DECLINED : APPROVED;

            if($user_billing_account_details->save()) {

                DB::commit();

                $message = $user_billing_account_details->status ? tr('user_account_approve_success') : tr('user_account_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method resolution_center_index()
     *
     * @uses view the users details based on users id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param integer $user_id
     * 
     * @return View page
     *
     */
    public function resolution_center_index(Request $request) {
        
        try {  

            // @todo align the code & redirect table list like disputes 

            $sub_page = 'resolution-center-all';

            $base_query  = UserDispute::join('users as disputesender','disputesender.id','=','user_disputes.user_id')
                            ->join('users as disputereceiver','disputereceiver.id','=','user_disputes.receiver_user_id')
                            ->orderBy('user_disputes.created_at','DESC');

            if($request->search_key) {

                $base_query = $base_query
                        ->where('disputesender.name','LIKE','%'.$request->search_key.'%')
                        ->orWhere('disputereceiver.name','LIKE','%'.$request->search_key.'%');
            }

            if($request->type!='') {
                
                if($request->type == YES){

                    $sub_page = 'resolution-center-closed';

                    $base_query->where('user_disputes.status', '!=', DISPUTE_SENT);
                
                } else{

                    $sub_page = 'resolution-center-opened';

                    $base_query->where('user_disputes.status', DISPUTE_SENT);

                }

            }

            $user_disputes = $base_query->paginate(10);

            return view('admin.resolution-center.index')
                    ->with('main_page','resolution_center_management')
                    ->with('page', 'resolution-center') 
                    ->with('sub_page', $sub_page)
                    ->with('user_disputes', $user_disputes);
            
        } catch (Exception $e) {

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method user_wallet_payments_status()
     *
     * @uses verify for the user
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */
    public function user_wallet_payments_status(Request $request) {

        try {
            
            DB::beginTransaction();

            $user_wallet_payment_details = UserWalletPayment::find($request->user_wallet_payment_id);
            
            if(!$user_wallet_payment_details) {

                throw new Exception(tr('user_details_not_found'), 101);
                
            }

            $user_wallet_payment_details->is_admin_approved = $user_wallet_payment_details->is_admin_approved ? DECLINED : APPROVED;

            $user_wallet_payment_details->status = USER_WALLET_PAYMENT_PAID;

            if($user_wallet_payment_details->save()) {

                if($user_wallet_payment_details->is_admin_approved == APPROVED) {

                    PaymentRepo::user_wallet_update($user_wallet_payment_details);
                }

                $message = $user_wallet_payment_details->is_admin_approved ? tr('approved') : tr('declined');

                $transaction_message = tr('wallet_transaction_message');

                $transaction_message = str_replace("<%transaction_amount%>", formatted_amount($user_wallet_payment_details->paid_amount),$transaction_message);

                $transaction_message = str_replace("<%status%>",$message,$transaction_message);

                $email_data['subject'] = Setting::get('site_name');

                $email_data['page'] = "emails.users.wallets_bank_transfer_status";
    
                $email_data['data'] = $user_wallet_payment_details->user;
    
                $email_data['email'] = $user_wallet_payment_details->user->email ?? '';

                $email_data['message'] = $transaction_message;

                $this->dispatch(new SendEmailJob($email_data));

                DB::commit();
                
                $message = $user_wallet_payment_details->is_admin_approved ? tr('user_wallet_payment_approve_success') : tr('user_wallet_payment_decline_success');

                return redirect()->back()->with('flash_success', $message);

                }
            
            throw new Exception(tr('user_wallet_payment_approve_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method user_withdrawals_index()
     *
     * @uses list of user withdrawals
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */

    public function user_withdrawals_index(Request $request) {

        $base_query = \App\UserWithdrawal::orderBy('user_withdrawals.updated_at','DESC');
        

        if($request->search_key){

            $search_key = $request->search_key;

            $base_query = $base_query
                            ->whereHas('user', function ($query) use ($search_key) {
                                    $query
                                    ->where('users.name', 'like', '%' .$search_key . '%')
                                    ->orWhere('users.email', 'like', '%' .$search_key . '%')
                                    ->orWhere('users.mobile', 'like', '%' .$search_key . '%');
                            });
        }

        if($request->status!='') {

            $base_query->where('user_withdrawals.status',$request->status);
        }


        $user_withdrawals = $base_query->commonResponse()->paginate(10);
        
        return view('admin.withdrawals.index')
                        ->with('main_page','withdrawals_list')
                        ->with('user_withdrawals' , $user_withdrawals);

    }

    /**
     * @method user_withdrawals_view()
     *
     * @uses list of user withdrawals
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */

    public function user_withdrawals_view(Request $request) {
          
     try {

            $user_withdrawal_details = \App\UserWithdrawal::where('id',$request->user_withdrawal_id)->first();

            if(!$user_withdrawal_details) { 

                throw new Exception(tr('user_withdrawal_not_found'), 101);                
            }  

            $billing_account_details = \App\UserBillingAccount::where('user_id', $user_withdrawal_details->user_id)->first();

            return view('admin.withdrawals.view')
                    ->with('main_page','withdrawals_list')
                    ->with('billing_account_details', $billing_account_details)
                    ->with('user_withdrawal_details', $user_withdrawal_details);

        } catch(Exception $e) {


            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_withdrawals_paynow
     *
     * @uses checkout the amount which the user requested
     *
     * @created Ganesh
     *
     * @updated vithya R
     *
     * @param integer $user_withdraw_request_id
     * 
     * @return response success/failure message
     *
     **/
    public function user_withdrawals_paynow(Request $request) {

        try {

            DB::beginTransaction();

            $user_withdrawal_details = \App\UserWithdrawal::find($request->user_withdrawal_id);

            if(!$user_withdrawal_details) {

                throw new Exception(tr('user_withdrawal_not_found'), 101);
                
            }

            // check the status, whether payment is eligible

            if(!in_array($user_withdrawal_details->status, [WITHDRAW_INITIATED, WITHDRAW_ONHOLD])) {

                throw new Exception(tr('user_withdrawals_paynow_not_allowed'), 101);
            }

            $user_withdrawal_details->status = WITHDRAW_PAID;

            $user_withdrawal_details->paid_amount = $user_withdrawal_details->requested_amount;

            if($user_withdrawal_details->save()) {

                // Update the wallet history

                if($user_wallet_payment = \App\UserWalletPayment::find($user_withdrawal_details->user_wallet_payment_id)) {

                    $user_wallet_payment->status = USER_WALLET_PAYMENT_PAID;

                    $user_wallet_payment->save();

                    // Revert the requested amount to the wallet

                    PaymentRepo::user_wallet_update_withdraw_paynow($user_withdrawal_details->requested_amount, $user_withdrawal_details->user_id);
                }


                DB::commit();

                // @todo send notification to the user 

                $email_data['subject'] = Setting::get('site_name');

                $email_data['page'] = "emails.users.withdrawals-approve";
    
                $email_data['data'] = $user_withdrawal_details->user;
    
                $email_data['email'] = $user_withdrawal_details->user->email ?? '';

                $email_data['message'] = tr('user_withdraw_paid_description');
    
                $this->dispatch(new SendEmailJob($email_data));

                $message =  tr('user_withdraw_paid_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.user_withdrawals.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_withdrawals_decline
     *
     * @uses To delete the users details based on users id
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param integer $users_id
     * 
     * @return response success/failure message
     *
     **/
    public function user_withdrawals_decline(Request $request) {

        try {

            DB::beginTransaction();

            $user_withdrawal_details = \App\UserWithdrawal::find($request->user_withdrawal_id);
            
            if(!$user_withdrawal_details) {

                throw new Exception(tr('user_withdrawal_not_found'), 101);
                
            }

            if(in_array($user_withdrawal_details->status, [WITHDRAW_DECLINED, WITHDRAW_CANCELLED])) {

                throw new Exception(tr('user_withdrawals_request_already_cancelled'), 101);                
            }

            // check the status, whether payment is eligible

            if(!in_array($user_withdrawal_details->status, [WITHDRAW_INITIATED, WITHDRAW_ONHOLD])) {

                throw new Exception(tr('user_withdrawals_paynow_not_allowed'), 101);
            }

            $user_withdrawal_details->status = WITHDRAW_DECLINED;

            if($user_withdrawal_details->save()) {

                // Update the wallet history

                if($user_wallet_payment = \App\UserWalletPayment::find($user_withdrawal_details->user_wallet_payment_id)) {

                    $user_wallet_payment->status = USER_WALLET_PAYMENT_CANCELLED;

                    $user_wallet_payment->is_cancelled = YES;

                    $user_wallet_payment->cancelled_reason = $request->cancelled_reason ?: "Withdraw request cancelled by admin";

                    $user_wallet_payment->save();

                    // Revert the requested amount to the wallet

                    PaymentRepo::user_wallet_update_withdraw_cancel($user_withdrawal_details->requested_amount, $user_withdrawal_details->user_id);
                }

                DB::commit();


                $email_data['subject'] = Setting::get('site_name');

                $email_data['page'] = "emails.users.withdrawals-decline";
    
                $email_data['data'] = $user_withdrawal_details->user;
    
                $email_data['email'] = $user_withdrawal_details->user->email ?? '';

                $email_data['message'] = tr('user_withdraw_decline_description');
                
                $this->dispatch(new SendEmailJob($email_data));


                $message =  tr('user_withdraw_declined');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.user_withdrawals.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_disputes_index
     *
     * @uses To list the dispute users
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     * 
     * @return response success/failure message
     *
     **/
     
    public function user_disputes_index(Request $request){

        $base_query = UserDispute::orderBy('updated_at','desc');

        if($request->status!='') {
       
            $base_query->where('status',$request->status);

        }

        $user_disputes  = $base_query->paginate(10);
        
        return view('admin.user-disputes.index')
                    ->with('main_page','user_disputes')
                    ->with('user_disputes', $user_disputes);
    
    }

    /**
     * @method user_disputes_view
     *
     * @uses To view the dispute users details
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     * 
     * @return response success/failure message
     *
     **/

    public function user_disputes_view(Request $request) {

        try {

            $user_dispute_details = UserDispute::where('id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) { 

                throw new Exception(tr('user_dispute_details_not_found'), 101);                
            }

            $messages = \App\ResolutionCenter::where('user_dispute_id', $request->user_dispute_id)->orderBy('created_at', 'asc')->get();

            return view('admin.user-disputes.view')
                            ->with('main_page','user_disputes')
                            ->with('user_dispute_details', $user_dispute_details)
                            ->with('messages', $messages);

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }
    
    /**
     * @method user_disputes_approve
     *
     * @uses approve the dispute
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return response success/failure message
     *
     **/

    public function user_disputes_approve(Request $request) {

        try {
            
            DB::beginTransaction();

            $user_dispute_details = UserDispute::where('id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) { 

                throw new Exception(tr('user_dispute_details_not_found'), 101);                
            }

            $user_dispute_details->status = DISPUTE_APPROVED;
            
            if($user_dispute_details->save()) {

                // Wallet update for case winner & loser

                PaymentRepo::user_wallet_update_dispute_approve($user_dispute_details->amount, $user_dispute_details->user_id, $user_dispute_details->receiver_user_id);

                DB::commit();

                // @todo sent mail notification

                return redirect()->back()->with('flash_success', tr('user_disputes_approved'));

            }
            
            throw new Exception(tr('user_disputes_approve_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_disputes_reject
     *
     * @uses approve the dispute
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return response success/failure message
     *
     **/

    public function user_disputes_reject(Request $request) {

        try {
            
            DB::beginTransaction();

            $user_dispute_details = UserDispute::where('id', $request->user_dispute_id)->first();

            if(!$user_dispute_details) { 

                throw new Exception(tr('user_dispute_details_not_found'), 101);                
            }

            $user_dispute_details->status = DISPUTE_DECLINED;
            
            if($user_dispute_details->save()) {

                // Wallet update for case winner & loser

                PaymentRepo::user_wallet_update_dispute_reject($user_dispute_details->amount, $user_dispute_details->receiver_user_id);

                DB::commit();

                // @todo sent mail notification

                return redirect()->back()->with('flash_success', tr('user_disputes_rejected'));

            }
            
            throw new Exception(tr('user_disputes_rejecte_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method user_disputes_message_save
     *
     * @uses To view the dispute users details
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     * 
     * @return response success/failure message
     *
     **/

    public function user_disputes_message_save(Request $request) {

        try {
            
            DB::beginTransaction();

            $resolution_center = new ResolutionCenter;

            $resolution_center->sender_id = $resolution_center->receiver_id  = 0;

            $resolution_center->message = $request->message ?: "";

            $resolution_center->message_type = DISPUTE_ADMIN_TO_USER;

            $resolution_center->user_dispute_id = $request->user_dispute_id;

            $resolution_center->user_wallet_payment_id = $request->user_wallet_payment_id;
            
            if($resolution_center->save()) {

                DB::commit();

                // @todo sent mail notification

                return redirect()->back()->with('flash_success', tr('resolution_center_sent'));

            }
            
            throw new Exception(tr('resolution_center_sent_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method inboxes_view()
     *
     * @uses view the inboxes based on users dispute id
     *
     * @created Ganesh 
     *
     * @updated 
     *
     * @param integer $user_id
     * 
     * @return View page
     *
     */

    public function inboxes_view(Request $request){

        $inboxes =  Inbox::where('user_dispute_id',$request->user_dispute_id)->get();

        foreach($inboxes as $inbox){

        $inboxes->sender = \App\User::find($inbox->sender_id) ?? '';

        }

        $view = view('admin.resolution-center._inboxes')->with('inbox',$inboxes)->render();
        
        return response()->json(array('data' => $view));

    }

    /**
     * @method env_settings_save()
     *
     * @uses used to update the email details for .env file
     *
     * @created Ganesh
     *
     * @updated
     *
     * @param Form data
     *
     * @return view page
     */

    public function env_settings_save(Request $request) {

        try {

            $env_settings = ['MAIL_DRIVER','MAIL_HOST', 'MAIL_PORT' , 'MAIL_USERNAME' , 'MAIL_PASSWORD' , 'MAIL_ENCRYPTION' , 'MAILGUN_DOMAIN' , 'MAILGUN_SECRET' , 'FCM_SERVER_KEY', 'FCM_SENDER_ID' , 'FCM_PROTOCOL', 'MAIL_FROM_ADDRESS', 'MAIL_FROM_NAME'];

            if($env_settings){

                foreach ($env_settings as $key => $data) {

                    if($request->$data){ 

                        \Enveditor::set($data,$request->$data);

                    } else{

                        \Enveditor::set($data,$request->$data);
                    }
                }
            }


            $message = tr('settings_update_success');

            return redirect()->route('clear-cache')->with('flash_success', $message);  

           
        } catch(Exception $e) {

            $error = $e->getMessage();
            
            return back()->withInput()->with('flash_error' , $error);

        }  

    }


    /**
     * @method users_bulk_action()
     * 
     * @uses To delete,approve,decline multiple users
     *
     * @created Ganesh
     *
     * @updated 
     *
     * @param 
     *
     * @return success/failure message
     */
    public function users_bulk_action(Request $request) {

        try {
            
            $action_name = $request->action_name ;

            $user_ids = explode(',', $request->selected_users);

            $message = '';

            if (!$user_ids && !$action_name) {

                throw new Exception(tr('user_action_is_empty'));

            }

            DB::beginTransaction();

            if($action_name == 'bulk_delete'){

                $user = User::whereIn('id', $user_ids)->delete();

                if (!$user) {

                    throw new Exception(tr('user_delete_failed'));

                }

                $message = tr('admin_users_delete_success');


            }elseif($action_name == 'bulk_approve'){

                $user =  User::whereIn('id', $user_ids)->update(['status' => USER_APPROVED]);

                if (!$user) {

                    throw new Exception(tr('users_approve_failed'));  

                }

                $message = tr('admin_users_approve_success');


            }elseif($action_name == 'bulk_decline'){
                
                $user =  User::whereIn('id', $user_ids)->update(['status' => USER_DECLINED]);

                if (!$user) {
                    
                    throw new Exception(tr('users_decline_failed')); 

                }

                $message = tr('admin_users_decline_success');

            }

            if($user){

                DB::commit();

                return back()->with('flash_success',$message)->with('bulk_action','true');

            }

        }catch( Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error',$e->getMessage());
        }

    }
}
